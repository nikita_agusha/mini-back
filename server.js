const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config")
const gallery = require("./app/gallery")

const app = express();
const PORT = 8000;


app.use(cors());
app.use(express.static("public"));

mongoose.connect(`${config.db.url}/${config.db.name}`, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("mongoose connected!");
        app.use("/gallery", gallery());
        app.listen(PORT, () => {
            console.log("Server started at http://localhost:" + PORT);
        });
    });