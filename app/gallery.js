const express = require("express");
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Gallery = require("../models/Gallery");
const router = express.Router();


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = (db) => {
    router.get("/", async (req, res) => {
        try {
            const gallery = await Gallery.find().populate("user")
            res.send(gallery)
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.get("/:id", async (req, res) => {
        console.log(req.params.id)
        const album = await
            Gallery.find({_id: req.params.id})
        res.send(album);
    });
    router.post("/", upload.single("image"), async (req, res) => {
        const gallery = new Gallery(req.body);
        if (req.file) {
            gallery.image = req.file.filename
        }
        await gallery.save();
        res.send(gallery)
    })
    router.delete("/:id", async (req, res) => {
        const deletedPhoto = await Gallery.remove({_id: req.params.id});
        res.send(deletedPhoto);
    });
    return router
}
module.exports = createRouter