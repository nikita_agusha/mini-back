const mongoose = require("mongoose");
const config = require("./config");
const Gallery = require("./models/Gallery")


mongoose.connect(`${config.db.url}/${config.db.name}`);
const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("gallery");
        await db.dropCollection("user");
    } catch (e) {
        console.log("collection  were not  presented. Skipping drop ...");
    }

    await Gallery.create({
        namePicture: "Good picture",
        image: "eminem-1.jpg",
    }, {
        namePicture: "Bad picture",
        image: "50-cent.jpg",
    }, {
        namePicture: "Bad picture",
        image: "eminem-1.jpg",
    }, {
        namePicture: "Good picture",
        image: "50-cent-Best-Of.jpg",
    }, {
        namePicture: "Bad picture",
        image: "50-cent-get-rich-die-tryin.jpg",
    }, {
        namePicture: "Bad picture",
        image: "eminem-1.jpg",
    }, {
        namePicture: "Good picture",
        image: "50-cent.jpg",
    });

    db.close()
})