const permit = (...roles) => {
    return (req, res, next) => {
        if (!req.user) {
            res.status(401).send("Unauthenticated")
        }
        if (!roles.includes(req.user.role)) {
            return res.status(405).send("Unauthorized")
        }
        next()
    }
}
module.exports= permit